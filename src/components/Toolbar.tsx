import React, { cloneElement, useState } from "react";
import clsx from "clsx";
import ToolbarMaterial from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import FilterListIcon from "@material-ui/icons/FilterList";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { useToolbarStyles } from "./style";
import AddIcon from "@material-ui/icons/Add";
import { Box } from "@material-ui/core";
import { IGrid } from "./type";

interface Props extends IGrid{
  selected: string[];
  setSelected:(data:string[])=>void;
  variablesOther:any
  setVariablesOther:any
}

const Toolbar = (props: Props) => {
  const [visibleFilter,setVisibleFilter] = useState(true);
  const classes = useToolbarStyles();
  const { selected, title, remove, setSelected,filter,add } = props;
  const {variablesOther,setVariablesOther} = props;
  const {length} = selected;
  const handleClickDelete = () => {
    remove && remove(selected);
    setSelected([]);
  };
  return (
    <>
    <ToolbarMaterial
      className={clsx(classes.root, {
        [classes.highlight]: length > 0,
      })}
    >
      <div className={classes.title}>
        {length > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {length} zaznaczone
          </Typography>
        ) : (
          <Typography variant="h6" id="tableTitle">
            {title}
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {length > 0 ? (
          <Tooltip title="Delete">
            <IconButton onClick={handleClickDelete} aria-label="delete">
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <div style={{ display: "flex" }}>
            {add && (
              <IconButton onClick={()=>console.log("add")} aria-label="Dodaj">
                <AddIcon />
              </IconButton>
            )}

              <Tooltip title="Filter list">
                <IconButton onClick={()=>setVisibleFilter(!visibleFilter)} aria-label="filter list">
                  <FilterListIcon />
                </IconButton>
              </Tooltip>
        </div>
        )}
      </div>
    </ToolbarMaterial>
    {visibleFilter &&
      <Box margin="0 15px">
        {cloneElement(filter || <> </>, {data:variablesOther,setData:setVariablesOther})}
      </Box>
    }
    </>
  );
};

export default Toolbar;
