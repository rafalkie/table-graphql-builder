import React, { useState } from "react";
import "../App.css";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import Grid from "./Grid";
import Column from "./Column";
import { IFilters } from "./type";

function App() {
  const client = new ApolloClient({
    uri: "http://localhost:3099/graphql",
  });

  const variables = [{ name: "id", type: "String", default: "" }];
  const config = {
    add: () => console.log("add"),
    variables,
    title: "Test",
    resource: "product",
    primaryKey: "_id",
  };
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <Grid {...config} filter={<Filter />}>
          <Column resource="_id" name="id" />
          <Column resource="key" name="klucz" />
          <Column resource="data" name="dane" />
        </Grid>
      </ApolloProvider>
    </div>
  );
}

export default App;

function Filter(props: IFilters) {
  const { data, setData } = props;
  return (
    <>
      <input
        defaultValue={data && data["id"]}
        onBlur={e => setData!({ id: e.target.value })}
      />
    </>
  );
}
