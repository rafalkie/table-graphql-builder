import { gql } from "apollo-boost";
import objectPath from "object-path";
import { ReactElement } from "react";
function isEmpty(obj: any) {
  return Object.getOwnPropertyNames(obj).length === 0;
}

interface Props {
  children: ReactElement[];
  resource: string;
  variables?: { name: string; type: string }[];
}

function queryBuilder(props: Props) {
  const { resource, children, variables } = props;
  const params = {};
  let paramsText = "";

  children?.map((item) => {
    if (item.props.resource) {
      objectPath.set(params, `.${resource}.${item.props.resource}`, {});
    }
  });
  //Szukanie ostatniego obiektu - potrzebny jeśli mamy nadrzędne obiekty
  // a ostatni zawiera specyfikacje typów
  let primaryObject = resource.split(/\.(?=[^\.]+$)/)[1] || resource;

  //Buduje np. ($cursor: String)
  if (variables) {
    paramsText += primaryObject.toUpperCase() + " ";
    paramsText += "(";
    variables.map((item, index) => {
      paramsText += `$${item.name}: ${item.type}`;
      if (variables.length !== index + 1) {
        paramsText += ",";
      }
    });
    paramsText += ")";
  }

  const searchChild = (data: any) => {
    Object.keys(data).map(item => {

      //Buduje np. (cursor: $cursor)
      if (variables && item === primaryObject) {
        paramsText += ` ${item} (`;
        variables.map((item, index) => {
          paramsText += `${item.name}: $${item.name} `;
          if (variables.length !== index + 1) {
            paramsText += ",";
          }
        });
        paramsText += ")";
      } else {
        paramsText += item + " ";
      }
      const next = data[item];
      if (!isEmpty(next)) {
        paramsText += " {";
        searchChild(next);
        paramsText += "} ";
      }
    });
  };
  searchChild(params);

  const query = gql`query ${paramsText}`;

  return query;
}

export default queryBuilder;
