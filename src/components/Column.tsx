import React, {  ReactChild } from "react";

export interface ColumnProps {
  resource?: string;
  name?: string;
  children?: ReactChild;
  item?:any;
  getData?:any;
  selected?:string[];
  setSelected?:(data:string[])=>void;
}

function Column(props: ColumnProps) {
  return props.children ? <>{props.children}</> : null
}

export default Column;
