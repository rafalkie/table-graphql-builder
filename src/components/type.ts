import { ReactElement } from "react";

export interface IGrid {
  children: ReactElement[];
  filter?: any;
  resource: string;
  getData?: any; // callback zwraca wszystkie metody i dane useQuery
  primaryKey:string;
  pagination?:boolean;
  variables?:{name:string,type:string,default:any}[];
  title?:string;
  remove?:(data:string[])=>void;
  add?:any; // callback
}

export interface IPagination {
  first: number;
  offset: number;
  setFirst: (data: number) => void;
  setOffset: (data: number) => void;
}

export interface IFilters {
  data?:any,
  setData?:(data:any)=>void
}
