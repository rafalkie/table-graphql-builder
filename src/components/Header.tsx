import React, { ReactElement } from "react";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import { Checkbox } from "@material-ui/core";

interface Props {
  children: ReactElement[];
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void;
  length: number;
  rowCount: number;
}

function Header({ children,onSelectAllClick,length,rowCount }: Props) {
  return (
    <TableHead>
      <TableRow>
      <TableCell padding="checkbox">
          <Checkbox
             indeterminate={length > 0 && length < rowCount}
             checked={rowCount > 0 && length === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell>
        {children?.map((item, index) => {

          const { name, resource } = item.props;

          return(
            <TableCell key={index}>
              {name ? name : resource}
            </TableCell>
            )

        })}
      </TableRow>
    </TableHead>
  );
}

export default Header;
