import React, { cloneElement } from "react";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import Paper from "@material-ui/core/Paper";
import Header from "./Header";
import { Checkbox } from "@material-ui/core";
import Pagination from "./Pagination";
import Toolbar from "./Toolbar";
import { useStylesBody } from "./style";
import { IGrid,IPagination } from "./type";

interface Props extends IGrid,IPagination {
  getData: any;
  variablesOther:any
  setVariablesOther:any
}

function Body(props: Props) {
  const { children, getData, resource, primaryKey,title } = props;
  const classes = useStylesBody();
  const { data } = getData;
  const [selected, setSelected] = React.useState<string[]>([]);

  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = data[resource].map((n: any) => n[primaryKey]);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: string[] = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    setSelected(newSelected);
  };
  return (
    <Paper className={classes.paper}>
      <Toolbar
        {...props}
        setSelected={setSelected}
        selected={selected}
      />
      <TableContainer>
        <Table className={classes.table}>
          <Header
            children={children}
            length={selected.length}
            rowCount={data && data[resource]?.length}
            onSelectAllClick={handleSelectAllClick}
          />

          <TableBody>
          { getData.error ?
            null
          :
            <>
            {data && data[resource].map((item: any, index: number) => {
              const isItemSelected = isSelected(item[primaryKey]);
              const labelId = `enhanced-table-checkbox-${index}`;

              return (
                <TableRow
                  hover
                  tabIndex={-1}
                  key={index}
                  role="checkbox"
                  aria-checked={isItemSelected}
                  selected={isItemSelected}
                >
                  <TableCell
                    onClick={event => handleClick(event, item[primaryKey])}
                    padding="checkbox"
                  >
                    <Checkbox
                      checked={isItemSelected}
                      inputProps={{ "aria-labelledby": labelId }}
                    />
                  </TableCell>
                  {children?.map((child,index) => (
                    <TableCell key={index}>
                      {cloneElement(child, {
                        item,
                        getData,
                        selected,
                        setSelected,
                      })}
                      {item[child.props.resource]}
                    </TableCell>
                  ))}
                </TableRow>
              );
            })}
            </>}
          </TableBody>
        </Table>
      </TableContainer>
      <Pagination {...props} />
    </Paper>
  );
}

export default Body;
