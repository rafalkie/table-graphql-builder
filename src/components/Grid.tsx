import React, { ReactElement, useEffect, useState } from "react";
import { useQuery, useLazyQuery } from "@apollo/react-hooks";
import Body from "./Body";
import queryBuilder from "./queryBuilder";
import { IGrid } from "./type";

interface Props extends IGrid{
  first?:number;
  offset?:number;
}
const variablesPagination = [
  {name:"first",type:"Int"},
  {name:"offset",type:"Int"}
];

function Grid(props: Props) {
  const { resource, children,pagination = true,filter,add } = props;
  const [first,setFirst] = useState(props.first || 5);
  const [offset,setOffset] = useState(props.offset || 0);
  const [variablesOther,setVariablesOther] = useState<{[key:string]:any}>();
  const mergeVariable = pagination ? [...props.variables || [],...variablesPagination] : props.variables;

  const query = queryBuilder({ resource, children,variables:mergeVariable });
  const variablesProp =
    props.variables
    ? { variables: variablesOther}
    : {} ;

  const allVariables = pagination ? { variables: { first,offset,...variablesOther}} : variablesProp;
  const getData = useQuery(query,{...allVariables} );

  useEffect(() => {
    if(props.variables){
      let helperVariable:{[key:string]:any} = {};
      let anyVariables:any = {};
      props.variables?.forEach(item=>{
        anyVariables =  {...anyVariables,...{[item.name]: item.default}};
        helperVariable =  {...helperVariable,...{[item.name]: anyVariables[item.name]}}
      });
      setVariablesOther(anyVariables || {});
   }
  }, [props.variables]);

  useEffect(() => {
    props.getData && props.getData(getData);
  }, []);

  console.log(getData);
  return (
    <>
      <Body
        {...props}
        getData={getData}
        pagination={pagination}
        filter={filter}
        {...{first,offset,setOffset,setFirst,variablesOther,setVariablesOther,add}}
      />
    </>
  );
}

export default Grid;
