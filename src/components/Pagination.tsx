import React from "react";
import TablePagination from "@material-ui/core/TablePagination";
import { IGrid,IPagination } from "./type";


interface Props extends IGrid, IPagination {}

function Pagination(props: Props) {
  const { offset, first, setOffset, setFirst, pagination } = props;

  const handleChangePage = (event: any, newPage: any) => {
    setOffset(newPage * first);
  };

  const handleChangeRowsPerPage = (event: any) => {
    setFirst(event.target.value);
  };

  const count = 10;
  return (
    <>
      {pagination && (
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50, 100]}
          labelRowsPerPage="Rzędów na strone"
          component="div"
          count={count}
          rowsPerPage={first}
          page={offset !== 0 ? offset / first : offset}
          backIconButtonProps={{
            "aria-label": "poprzednia strona"
          }}
          nextIconButtonProps={{
            "aria-label": "następna strona"
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      )}
    </>
  );
}

export default Pagination;
